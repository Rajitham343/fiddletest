package net.dotnetfiddle.acceptancetests;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources", glue = { "net.dotnetfiddle" }, plugin = { "pretty",
		"html:target/cucumber-reports", "json:target/cucumber-reports/cucumber.json" }, monochrome = true, tags = { "@Automation" })
public class TestRunner {

}
