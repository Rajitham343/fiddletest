package net.dotnetfiddle.hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import net.dotnetfiddle.utils.BrowserUtils;

public class Hooks {

	@Before("@hook-Init")
	public void initializations() throws Throwable {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		BrowserUtils.setDriver();
	}
	
	@After("@hook-Cleanup")
	public void cleanUp() throws Throwable {
		BrowserUtils.getWebDriver().close();
	}
}
