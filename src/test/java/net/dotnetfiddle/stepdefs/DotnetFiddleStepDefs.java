package net.dotnetfiddle.stepdefs;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.HeadlessException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.dotnetfiddle.pageobjects.DotnetFiddlePage;
import net.dotnetfiddle.utils.BrowserUtils;

public class DotnetFiddleStepDefs {

	DotnetFiddlePage dotnetFiddlePage = new DotnetFiddlePage();

	@Given("I am on the Dotnetfiddle Application page")
	public void i_am_on_the_Dotnetfiddle_Application_page() {
		BrowserUtils.getWebDriver().get("https://dotnetfiddle.net/");
		BrowserUtils.getWebDriver().manage().window().maximize();
	}

	@When("I click on the Run button")
	public void i_click_on_the_Run_button() {
		dotnetFiddlePage.clickRunButton();
	}

	@Then("I should see the {string} text in the output console")
	public void i_should_see_the_text_in_the_output_console(String outputText) {
		System.out.println("output text: " + dotnetFiddlePage.getTextFromOutputConsole());
		Assert.assertEquals(outputText, dotnetFiddlePage.getTextFromOutputConsole());
	}

	@When("your {string} starts with {string}")
	public void your_starts_with(String name, String pattern) {
		Character firstLetter = name.charAt(0);
		switch (Character.toUpperCase(firstLetter)) {
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
			dotnetFiddlePage.enterNuGetText("nUnit").selectNuGetVersion("3.12.0.0");
			break;
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
			dotnetFiddlePage.clickShareButton();
			break;
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
			dotnetFiddlePage.clickOptionsButton();
			break;
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
			dotnetFiddlePage.clickSaveButton();
			break;
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
			dotnetFiddlePage.clickGettingStartedButton();
			break;
		default:
			System.out.println("Invalid name");
			break;
		}
		BrowserUtils.waitForPageLoaded();
	}

	@Then("I should see that nUnit package is added")
	public void i_should_see_that_nUnit_package_is_added() {
		assertTrue(dotnetFiddlePage.isNuGetPackageAdded("3.12.0.0"));
	}

	@Then("I should see that link starts with {string}")
	public void i_should_see_that_link_starts_with(String link)
			throws HeadlessException, UnsupportedFlavorException, IOException {
		System.out.println("output text: " + dotnetFiddlePage.getShareLinkTextBox());
		assertTrue(dotnetFiddlePage.getShareLinkTextBox().startsWith(link));
	}

	@Then("I should see that Options panel is hidden")
	public void i_should_see_that_Options_panel_is_hidden() {
		assertFalse(dotnetFiddlePage.isSidePannelDisplayed());
	}

	@Then("I should see that Log In window appeared")
	public void i_should_see_that_Log_In_window_appeared() {
		assertTrue(dotnetFiddlePage.isLoginPopupDisplayed());
	}

	@Then("I should see that {string} button appeared")
	public void i_should_see_that_button_appeared(String string) {
		assertTrue(dotnetFiddlePage.isbackToEditorButtonDisplayed());
	}
}
