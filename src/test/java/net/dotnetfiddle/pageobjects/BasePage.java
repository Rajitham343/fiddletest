package net.dotnetfiddle.pageobjects;

import org.openqa.selenium.support.PageFactory;

import net.dotnetfiddle.utils.BrowserUtils;

public class BasePage {

	public BasePage() {
		super();
		PageFactory.initElements(BrowserUtils.getWebDriver(), this);
	}
}
