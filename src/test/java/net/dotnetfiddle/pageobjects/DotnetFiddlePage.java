package net.dotnetfiddle.pageobjects;

import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

import net.dotnetfiddle.utils.BrowserUtils;

public class DotnetFiddlePage extends BasePage {
	
	@FindBy(how = How.ID, using = "run-button")
	public WebElement runButton;
	
	@FindBy(how = How.ID, using = "output")
	public WebElement outputConsole;
	
	@FindBy(how = How.CSS, using = "input.new-package")
	public WebElement nuGetTextbox;
	
	@FindBy(how = How.CSS, using = "a[package-id='NUnit']:nth-child(1)")
	public WebElement nUnitPackage;
	
	@FindBy(how = How.XPATH, using = "//div[@package-id='NUnit']")
	public WebElement nUnitAfterAdded;
	
	@FindBy(how = How.CSS, using = "a[package-id='NUnit']:nth-child(1)+ul")
	public WebElement nUnitPackageVersionsList;
	
	@FindBy(how = How.ID, using = "Share")
	public WebElement shareButton;
	
	@FindBy(how = How.ID, using = "ShareLink")
	public WebElement shareLinkTextBox;
	
	@FindBy(how = How.CSS, using = "a.copy-clipboard")
	public WebElement copySharedLink;
	
	@FindBy(how = How.CSS, using = "button.btn-sidebar-toggle")
	public WebElement optionsButton;
	
	@FindBy(how = How.ID, using = "save-button")
	public WebElement saveButton;
	
	@FindBy(how = How.ID, using = "login-modal")
	public WebElement loginPopUp;
	
	@FindBy(how = How.CSS, using = "a[href='/GettingStarted/']")
	public WebElement gettingStartedButton;
	
	@FindBy(how = How.XPATH, using = "//span[contains(@class, 'glyphicon')]/parent::a[@href='/']")
	public WebElement backToEditorButton;

	public void clickRunButton() {
		//BrowserUtils.waitForPageLoaded();
		runButton.click();
	}
	
	public String getTextFromOutputConsole() {
		//BrowserUtils.waitForPageLoaded();
		return outputConsole.getText();
	}
	
	public DotnetFiddlePage enterNuGetText(String text) {
		nuGetTextbox.sendKeys(text);
		return this;
	}
	
	public DotnetFiddlePage selectNuGetVersion(String version) {
		BrowserUtils.waitForPageLoaded();
        Actions actions = new Actions(BrowserUtils.getWebDriver());
    	actions.moveToElement(nUnitPackage).perform();    	
    	
    	this.waitUntilPackageListVisible(); 	
    	
    	WebElement nUnitPackageVersion = BrowserUtils.getWebDriver()
    			.findElement(By.cssSelector("a[version-name='"+version+"']"));    	
    	nUnitPackageVersion.click();
		return this;
	}
	
	public Boolean isNuGetPackageAdded(String version) {		
    	return nUnitAfterAdded.isDisplayed();
	}

	public void clickShareButton() {
		shareButton.click();
	}
	
	public String getShareLinkTextBox() throws HeadlessException, UnsupportedFlavorException, IOException {
		copySharedLink.click();
		String localClipboardData = (String) Toolkit.getDefaultToolkit()
                .getSystemClipboard().getData(DataFlavor.stringFlavor);	
		return localClipboardData;
	}
	
	public void clickOptionsButton() {
		optionsButton.click();
	}
	
	public Boolean isSidePannelDisplayed() {
		return nuGetTextbox.isDisplayed();
	}
	
	public void clickSaveButton() {
		saveButton.click();
	}
	
	public Boolean isLoginPopupDisplayed() {
		return loginPopUp.isDisplayed();
	}
	
	public void clickGettingStartedButton() {
		gettingStartedButton.click();
	}
	
	public Boolean isbackToEditorButtonDisplayed() {
		return backToEditorButton.isDisplayed();
	}
	
	private void waitUntilPackageListVisible() {
		FluentWait wait = new FluentWait(BrowserUtils.getWebDriver());
    	wait.withTimeout(Duration.ofMinutes(1));
    	wait.pollingEvery(Duration.ofMillis(5000));
    	wait.until(ExpectedConditions.visibilityOf(nUnitPackageVersionsList)); 
	}
	
}
