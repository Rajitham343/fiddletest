@hook-Init @hook-Cleanup 
Feature: Dotnetfiddle Application testing 
	As a Dotnetfiddle Application consumer
  I want to be able to navigate to Dotnetfiddle Application and use all the features of the application

@Automation @Test1 
Scenario: Validate the Run button functionality 
	Given I am on the Dotnetfiddle Application page 
	When I click on the Run button 
	Then I should see the "Hello World" text in the output console 
	
@Automation @Test1 
Scenario Outline: Validate the Run button functionality 
	Given I am on the Dotnetfiddle Application page 
	When your "<name>" starts with "<pattern>" 
	Then I should see that <response> 
	Examples: 
		| name | pattern |response|
		| Adam | A-B-C-D-E |nUnit package is added|
		| John | F-G-H-I-J-K |link starts with "https://dotnetfiddle.net/"|
		| Michael | L-M-N-O-P |Options panel is hidden|
		| Rockey | Q-R-S-T-U |Log In window appeared|
		| Virat | V-W-X-Y-Z |"< Back to Editor" button appeared|