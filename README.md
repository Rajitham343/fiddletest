###Before Test:
Open browser
Navigate to https://dotnetfiddle.net/

### Tests
Test 1:
Click “Run” button
Check Output window (“Hello World” text is expected)

Test 2 (If your first name starts with letter “A-B-C-D-E”): 
Select NuGet Packages: nUnit (3.12.0)
Check that nUnit package is added

Test 2 (If your first name starts with letter “F-G-H-I-J-K”): 
Click “Share” button
Check that link starts with “https://dotnetfiddle.net/”


Test 2 (If your first name starts with letter “L-M-N-O-P”):
Click “<” button on “Options” panel to hide this panel
Check that “Options” panel is hidden

Test 2 (If your first name starts with letter “Q-R-S-T-U”):
Click “Save” button
Check that “Log In” window appeared

Test 2 (If your first name starts with letter “V-W-X-Y-Z”):
Click “Getting Started” button
Check that “< Back to Editor” button appeared


###Requirements
Language: C# or Java (C# is preferred)
Automation tool: Selenium WebDriver
Assertions in tests
Whatever you like to demonstrate best practices (Page Object model, etc)

Used Chrome driver 102 version

###Execution
mvn clean install
